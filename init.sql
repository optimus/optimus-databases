CREATE DATABASE IF NOT EXISTS `server` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `server`;

CREATE TABLE IF NOT EXISTS `services` (
  `name` varchar(32) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `manifest` json DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `server`.`services` VALUES ('optimus-databases', 1,JSON_COMPACT(LOAD_FILE('/srv/manifest.json')));